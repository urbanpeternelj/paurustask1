USE [paurus]
GO

/****** Object:  Table [dbo].[TreaderInfo]    Script Date: 30/08/2021 18:24:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TraderInfo](
	[traderId] [int] unique NOT NULL,
	[playedAmount] [float] NOT NULL,
	[odd] [float] NOT NULL
) ON [PRIMARY]
GO