package si.paurus.trader.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import si.paurus.trader.conf.AppConfig;
import si.paurus.trader.datasource.HikariDS;
import si.paurus.trader.dto.IncomingDTO;
import si.paurus.trader.dto.OutgoingDTO;

@Service
public class TraderService implements ITraderService {

	@Autowired
	AppConfig conf;

	/** settings related to trader - set in configuration file */
	private static float taxationByRate;
	private static float taxationAmount;
	private static boolean isTaxationByRate;

	@Override
	public OutgoingDTO returnPotentialProfits(IncomingDTO traderDTO) throws Exception {

		setConfigs();

		insertData(traderDTO);

		// calculate parameters
		float fullReturnAmount = calculateReturnAmount(traderDTO.getPlayedAmount(), traderDTO.getOdd());
		float returnAmountAfterTax = isTaxationByRate
				? calculateTaxAmount(fullReturnAmount, calculateTaxRate(fullReturnAmount, taxationByRate))
				: calculateTaxAmount(fullReturnAmount, taxationAmount);
		float taxRate = isTaxationByRate ? taxationByRate : (1 - (returnAmountAfterTax / fullReturnAmount));

		OutgoingDTO outgoingDTO = setOutputData(fullReturnAmount, returnAmountAfterTax, taxRate);
		return outgoingDTO;
	}

	/** populate outgoing object */
	private OutgoingDTO setOutputData(float fullReturnAmount, float returnAmountAfterTax, float taxRate) {
		OutgoingDTO outgoingDTO = new OutgoingDTO();
		outgoingDTO.setPossibleReturnAmount(fullReturnAmount);
		outgoingDTO.setPossibleReturnAmountAfterTax(returnAmountAfterTax);
		outgoingDTO.setPossibleReturnAmountBefTax(fullReturnAmount);
		outgoingDTO.setTaxRate(taxRate);
		outgoingDTO.setTaxAmount(fullReturnAmount - returnAmountAfterTax);

		return outgoingDTO;
	}

	/** get attributes from configuration file */
	private void setConfigs() {
		isTaxationByRate = Boolean.parseBoolean(conf.getProp("isTaxRate"));
		taxationByRate = Float.parseFloat(conf.getProp("rate"));
		taxationAmount = Float.parseFloat(conf.getProp("amount"));
	}

	/** insert incoming data to database */
	public static int insertData(IncomingDTO traderDTO) throws SQLException {

		String getIdQuery = "INSERT INTO paurus.dbo.TraderInfo (traderId, playedAmount, odd) VALUES (?, ?, ?)";
		int result = 0;

		try (Connection con = HikariDS.getConnection(); PreparedStatement pst = con.prepareStatement(getIdQuery)) {
			pst.setInt(1, traderDTO.getTraderId());
			pst.setFloat(2, traderDTO.getPlayedAmount());
			pst.setFloat(3, traderDTO.getOdd());

			pst.execute();
		}
		return result;
	}

	/** calculate odd * played amount */
	private float calculateReturnAmount(float playedAmount, float odd) {
		return playedAmount * odd;
	}

	/** calculate tax rate */
	private float calculateTaxRate(float fullAmount, float taxRate) {
		return fullAmount * taxRate;
	}

	/** calculate tax amount in money */
	private float calculateTaxAmount(float fullAmount, float taxAmount) {
		return fullAmount - taxAmount;
	}

}
