package si.paurus.trader.service;

import si.paurus.trader.dto.IncomingDTO;
import si.paurus.trader.dto.OutgoingDTO;

public interface ITraderService {
	OutgoingDTO returnPotentialProfits(IncomingDTO traderDTO) throws Exception;
}
