package si.paurus.trader.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import si.paurus.trader.dto.IncomingDTO;
import si.paurus.trader.dto.OutgoingDTO;

@RestController
public class TraderController {

	@Autowired
	private ITraderService tradingService;

	@PostMapping(value = "/trade")
	public OutgoingDTO getProduct(@RequestBody IncomingDTO traderDTO) throws Exception {
		return tradingService.returnPotentialProfits(traderDTO);
	}
}
