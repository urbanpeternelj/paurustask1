package si.paurus.trader.conf;

import javax.annotation.Resource;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:config.properties")
public class AppConfig {

	@Resource
	public Environment env;

	public String getProp(String name) {
		return env.getProperty(name);
	}
}