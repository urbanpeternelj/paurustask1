package si.paurus.trader.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan({ "si.paurus.trader.service", "si.paurus.trader.datasource", "si.paurus.trader.conf"})
public class TraderApplicationStarter {
	public static void main(String[] args) {
		SpringApplication.run(TraderApplicationStarter.class, args);
	}
}
