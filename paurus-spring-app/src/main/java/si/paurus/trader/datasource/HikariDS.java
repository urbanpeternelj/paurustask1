package si.paurus.trader.datasource;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class HikariDS {

	private static HikariConfig config = new HikariConfig();
	private static HikariDataSource ds;

	static {
		config.setJdbcUrl("jdbc:sqlserver://localhost:1433;databaseName=paurus");
		config.setUsername("paurusUser");
		config.setPassword("Test1234");
		
		config.setDataSourceClassName("com.microsoft.sqlserver.jdbc.SQLServerDataSource");

		ds = new HikariDataSource(config);
	}

	private HikariDS() {
		//implement if necessary
	}

	public static Connection getConnection() throws SQLException {
		return ds.getConnection();
	}
}
