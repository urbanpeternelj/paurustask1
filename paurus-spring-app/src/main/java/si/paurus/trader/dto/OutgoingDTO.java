package si.paurus.trader.dto;

public class OutgoingDTO {

	private float possibleReturnAmount;
	private float possibleReturnAmountBefTax;
	private float possibleReturnAmountAfterTax;
	private float taxRate;
	private float taxAmount;

	public OutgoingDTO() {
		
	}

	public double getPossibleReturnAmount() {
		return possibleReturnAmount;
	}

	public void setPossibleReturnAmount(float possibleReturnAmount) {
		this.possibleReturnAmount = possibleReturnAmount;
	}

	public double getPossibleReturnAmountBefTax() {
		return possibleReturnAmountBefTax;
	}

	public void setPossibleReturnAmountBefTax(float possibleReturnAmountBefTax) {
		this.possibleReturnAmountBefTax = possibleReturnAmountBefTax;
	}

	public double getPossibleReturnAmountAfterTax() {
		return possibleReturnAmountAfterTax;
	}

	public void setPossibleReturnAmountAfterTax(float possibleReturnAmountAfterTax) {
		this.possibleReturnAmountAfterTax = possibleReturnAmountAfterTax;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(float taxRate) {
		this.taxRate = taxRate;
	}

	public double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(float taxAmount) {
		this.taxAmount = taxAmount;
	}
	
}