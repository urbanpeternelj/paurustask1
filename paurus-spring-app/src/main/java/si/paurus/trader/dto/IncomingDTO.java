package si.paurus.trader.dto;

public class IncomingDTO {

	private int traderId;
	private float playedAmount;
	private float odd;

	public IncomingDTO() {

	}

	public IncomingDTO(int traderId, float playedAmount, float odd) {
		// super();
		this.traderId = traderId;
		this.playedAmount = playedAmount;
		this.odd = odd;
	}

	public int getTraderId() {
		return traderId;
	}

	public void setTraderId(int traderId) {
		this.traderId = traderId;
	}

	public float getPlayedAmount() {
		return playedAmount;
	}

	public void setPlayedAmount(float playedAmount) {
		this.playedAmount = playedAmount;
	}

	public float getOdd() {
		return odd;
	}

	public void setOdd(float odd) {
		this.odd = odd;
	}

}